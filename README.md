## Name
Game project

## Add your files

cd existing_repo
git remote add origin https://gitlab.com/Melanie1234/project-game.git
git branch -M main
git push -uf origin main

La maquette du jeu est consultable sur https://www.figma.com/file/SA0fiIDnfWp9bkjJBML6Mn/project-game?node-id=0%3A1&t=UG7YhrtKxSwNNJOY-0

## Description
Ce jeu est un Memory composé de 3 niveaux de difficultés. Le niveau "facile" propose 12 cartes, le niveau "moyen" propose 18 cartes et enfin le niveau "difficile" se compose de 24 cartes. L'objectif est de trouver les paires avec le moins d'essais possible. En effet, chaque partie commence avec un score de 5 points. A chaque essai non fructueux, le joueur perd 1 point. Néanmoins, à chaque pair trouvée, le joueur gagne 3 points.

## Usage
Ce projet mobilise HTML & CSS, le framework Boostrap et TypeScript. Il est consultable sur https://gitlab.com/Melanie1234/project-game
Une fois le projet proche de son aboutissement, je l'ai déployé via Netlify cf. https://playful-melomakarona-028541.netlify.app/

## Support
En cas d'interrogations ou de difficultés de navigation, il est possible de me contacter à l'adresse : melanie.ferer@hotmail.fr

## Project status
Ce jeu est d'un point de vue technique terminé. Cependant, des amélioration relatives au design seraient à prévoir.
Aussi, toutes suggestions ou remarques sont les bienvenues.