import { Card } from "./entities";
import card1 from '../public/img/card1.png';
import card2 from '../public/img/card2.png';
import card3 from '../public/img/card3.png';
import card4 from '../public/img/card4.png';
import card5 from '../public/img/card5.png';
import card6 from '../public/img/card6.png';

import cardBack from '../public/img/back-card1.png';

import cardA from '../public/img/cardA.png';
import cardB from '../public/img/cardB.png';
import cardC from '../public/img/cardC.png';
import cardD from '../public/img/cardD.png';
import cardE from '../public/img/cardE.png';
import cardF from '../public/img/cardF.png';
import cardG from '../public/img/cardG.png';
import cardH from '../public/img/cardH.png';
import cardI from '../public/img/cardI.png';
import cardJ from '../public/img/cardJ.png';
import cardK from '../public/img/cardK.png';
import cardL from '../public/img/cardL.png';
import cardM from '../public/img/cardM.png';

import cat1 from '../public/img/cat1.png';
import cat2 from '../public/img/cat2.png';
import cat3 from '../public/img/cat3.png';
import cat4 from '../public/img/cat4.png';
import cat5 from '../public/img/cat5.png';
import cat6 from '../public/img/cat6.png';
import cat7 from '../public/img/cat7.png';
import cat8 from '../public/img/cat8.png';
import cat9 from '../public/img/cat9.png';
import cat10 from '../public/img/cat10.png';




const tabCartes1: Card[] = [
  { src: card1, alt: 'valet de trefle', isFlipped: false },
  { src: card2, alt: 'valet de trefle', isFlipped: false },
  { src: card1, alt: 'valet de trefle', isFlipped: false },
  { src: card2, alt: 'valet de trefle', isFlipped: false },
  { src: card3, alt: 'valet de trefle', isFlipped: false },
  { src: card4, alt: 'valet de trefle', isFlipped: false },
  { src: card3, alt: 'valet de trefle', isFlipped: false },
  { src: card4, alt: 'valet de trefle', isFlipped: false },
  { src: card5, alt: 'valet de trefle', isFlipped: false },
  { src: card6, alt: 'valet de trefle', isFlipped: false },
  { src: card5, alt: 'valet de trefle', isFlipped: false },
  { src: card6, alt: 'valet de trefle', isFlipped: false },

]

const tabCartes2: Card[] = [
  { src: cat1, alt: 'valet de trefle', isFlipped: false },
  { src: cat2, alt: 'valet de trefle', isFlipped: false },
  { src: cat3, alt: 'valet de trefle', isFlipped: false },
  { src: cat4, alt: 'valet de trefle', isFlipped: false },
  { src: cat5, alt: 'valet de trefle', isFlipped: false },
  { src: cat6, alt: 'valet de trefle', isFlipped: false },
  { src: cat7, alt: 'valet de trefle', isFlipped: false },
  { src: cat8, alt: 'valet de trefle', isFlipped: false },
  { src: cat9, alt: 'valet de trefle', isFlipped: false },
  { src: cat1, alt: 'valet de trefle', isFlipped: false },
  { src: cat2, alt: 'valet de trefle', isFlipped: false },
  { src: cat3, alt: 'valet de trefle', isFlipped: false },
  { src: cat4, alt: 'valet de trefle', isFlipped: false },
  { src: cat5, alt: 'valet de trefle', isFlipped: false },
  { src: cat6, alt: 'valet de trefle', isFlipped: false },
  { src: cat7, alt: 'valet de trefle', isFlipped: false },
  { src: cat8, alt: 'valet de trefle', isFlipped: false },
  { src: cat9, alt: 'valet de trefle', isFlipped: false },

]


const tabCartes3: Card[] = [
  { src: cardA, alt: 'valet de trefle', isFlipped: false },
  { src: cardB, alt: 'valet de trefle', isFlipped: false },
  { src: cardC, alt: 'valet de trefle', isFlipped: false },
  { src: cardD, alt: 'valet de trefle', isFlipped: false },
  { src: cardE, alt: 'valet de trefle', isFlipped: false },
  { src: cardF, alt: 'valet de trefle', isFlipped: false },
  { src: cardG, alt: 'valet de trefle', isFlipped: false },
  { src: cardH, alt: 'valet de trefle', isFlipped: false },
  { src: cardI, alt: 'valet de trefle', isFlipped: false },
  { src: cardJ, alt: 'valet de trefle', isFlipped: false },
  { src: cardK, alt: 'valet de trefle', isFlipped: false },
  { src: cardL, alt: 'valet de trefle', isFlipped: false },
  { src: cardA, alt: 'valet de trefle', isFlipped: false },
  { src: cardB, alt: 'valet de trefle', isFlipped: false },
  { src: cardC, alt: 'valet de trefle', isFlipped: false },
  { src: cardD, alt: 'valet de trefle', isFlipped: false },
  { src: cardE, alt: 'valet de trefle', isFlipped: false },
  { src: cardF, alt: 'valet de trefle', isFlipped: false },
  { src: cardG, alt: 'valet de trefle', isFlipped: false },
  { src: cardH, alt: 'valet de trefle', isFlipped: false },
  { src: cardI, alt: 'valet de trefle', isFlipped: false },
  { src: cardJ, alt: 'valet de trefle', isFlipped: false },
  { src: cardK, alt: 'valet de trefle', isFlipped: false },
  { src: cardL, alt: 'valet de trefle', isFlipped: false },
]


let divCardOfCards = document.querySelector<HTMLElement>("#cardOfCards")
if (divCardOfCards) {
  divCardOfCards.classList.add("hide")
}

let btnFacile = document.querySelector<HTMLButtonElement>("#facile")
let btnMoyen = document.querySelector<HTMLButtonElement>("#moyen")
let btnDifficile = document.querySelector<HTMLButtonElement>("#difficile")

let niveau = document.querySelector<HTMLElement>("#niveau")
let level = 0;

btnFacile?.addEventListener("click", () => {
  generateBoard(tabCartes1)
  hideOrDisplay()
  if (btnFacile && niveau) {
    niveau.innerHTML = " Facile";
    level = 1
  }
})

btnMoyen?.addEventListener("click", () => {
  generateBoard(tabCartes2)
  hideOrDisplay()
  if (btnMoyen && niveau) {
    niveau.innerHTML = " Moyen";
    level = 2
  }
})

btnDifficile?.addEventListener("click", () => {
  generateBoard(tabCartes3)
  hideOrDisplay()
  if (btnDifficile && niveau) {
    niveau.innerHTML = " Difficile";
    level = 3
  }
})


function hideOrDisplay() {
  let cardWelcome = document.querySelector<HTMLElement>("#cardWelcome")
  if (cardWelcome) {
    cardWelcome.classList.add("hide")
  }
  if (divCardOfCards) {
    divCardOfCards.classList.remove("hide")
    divCardOfCards.classList.add("display")
  }
}

/**
 * Fonction qui mélange les cartes 
 * @param arr 
 */
function shuffleArray(arr: any[]) {
  arr.sort(() => Math.random() - 0.5);
}

let cardOfCards = document.querySelector<HTMLElement>('#cardBodyOfCards')

/**
 * Fonction qui génère la table de jeu avec les cartes retournées 
 * @param cardTab 
 */
function generateBoard(cardTab: Card[]) {

  shuffleArray(cardTab);

  if (cardOfCards) {
    cardOfCards.innerHTML = ''
  }


  let flippedCards = 0;

  for (const oneCard of cardTab) {
    let img = document.createElement("img");

    img.className = "imgCardFront";
    cardOfCards?.append(img)

    if (oneCard.isFlipped) {
      img.src = oneCard.src;
    } else {
      img.src = cardBack;
    }

    img.addEventListener('click', () => {
      if (flippedCards >= 2) {
        return;
      }
      if (oneCard.isFlipped) {
        return;
      }
      flipping(oneCard, img);
      img.classList.add("flip");
      flippedCards++;

      setTimeout(() => {
        img.classList.remove("flip");
        flippedCards--;
      }, 900);
    });
  }
}

let score = 5
let scoreDisplay = document.querySelector("#score")

let flippedCards: Card[] = [];

/**
 * Fonction qui si la carte isFlipped montre la face, si non montre le dos
 * Si 2 cartes isFlipped, alors elles st stockées dans le tableau flippedCards
 * Si il y a 2 cartes dans flippedCard, alors fait comaraison entre les sources des 2 images
 * Si match elles restent retournées, si pas match elles se retournent au bout d'une seconde
 * Ds les 2 cas, flippedCard se vide à la fin de l'opération
 * @param card 
 * @param img 
 */
function flipping(card: Card, img: HTMLImageElement) {
  if (scoreDisplay) {
    scoreDisplay.innerHTML = String(score)
  }
  if (card.isFlipped) {
    img.src = cardBack;
  } else {

    img.src = card.src;
    flippedCards.push(card);
  }
  card.isFlipped = !card.isFlipped;

  if (flippedCards.length === 2) {
    const [card1, card2] = flippedCards;
    if (card1.src === card2.src) {
      // si match alors elles restent retournées

      flippedCards = [];
      score += 3

    } else {
      score -= 1
      // si pas de match alors elles se remettent de dos 
      setTimeout(() => {
        flippedCards.forEach((card) => {
          const img = document.querySelector(`img[src='${card.src}']`) as HTMLImageElement;
          img.src = cardBack;
          card.isFlipped = false;
        });
        flippedCards = [];
      }, 900); // retourne les cartes au bout d'une seconde

    }

    if (scoreDisplay) {
      scoreDisplay.innerHTML = String(score)
    }
  }
  if (tabCartes1 && level === 1) {
    flippedOrNotFlipped(tabCartes1)
  }
  else if (tabCartes2 && level == 2) {
    flippedOrNotFlipped(tabCartes2)

  }
  else if (tabCartes3 && level === 3) {
    flippedOrNotFlipped(tabCartes3)
  }

}

function flippedOrNotFlipped(tabCard: Card[]) {
  const allCardsFlipped = tabCard.every(tabCard => tabCard.isFlipped);

  if (allCardsFlipped) {
    createWinCard()
    if (cardOfCards) {
      cardOfCards.classList.add("hide")
    }
  }
  if (score <= 0) {
    createGameOverCard()
    if (cardOfCards) {
      cardOfCards.classList.add("hide")
    }

    if (scoreDisplay) {
      scoreDisplay.innerHTML = String(score)
    }
  }
}


function createGameOverCard() {
  let gameOVerCard = document.querySelector<HTMLDivElement>('#gameOverContainer')

  let gameOVer = document.createElement("div")
  gameOVer.classList.add('card');
  gameOVer.classList.add('resultCard');


  const cardTitle = document.createElement('h5');
  cardTitle.classList.add('card-title');
  cardTitle.textContent = "Game Over";

  const cardBody = document.createElement('div');
  cardBody.classList.add('card-body');

  const cardText = document.createElement('p');
  cardText.classList.add('card-text');
  cardText.textContent = "Tu feras mieux la prochaine fois ;)";

  const btnResart = document.createElement("button")
  btnResart.textContent = "Retour à la page d'accueil"
  btnResart.classList.add('btn_restart')

  cardBody.appendChild(cardTitle);
  cardBody.appendChild(cardText);
  cardBody.appendChild(btnResart);
  gameOVer.appendChild(cardBody);
  gameOVerCard?.append(gameOVer)

  if (btnResart) {
    btnResart.addEventListener("click", () => {
      location.reload()
    })
  }
}


function createWinCard() {
  let winCard = document.querySelector<HTMLDivElement>('#gameOverContainer')

  let victory = document.createElement("div")
  victory.classList.add('card');
  victory.classList.add('resultCard');

  const cardTitle = document.createElement('h5');
  cardTitle.classList.add('card-title');
  cardTitle.textContent = "Bravo";

  const cardBody = document.createElement('div');
  cardBody.classList.add('card-body');

  const cardText = document.createElement('p');
  cardText.classList.add('card-text');
  cardText.textContent = "Tu as trouvé toutes les cartes !";

  const btnResart = document.createElement("button")
  btnResart.textContent = "Retour à la page d'accueil"
  btnResart.classList.add('btn_restart')

  cardBody.appendChild(cardTitle);
  cardBody.appendChild(cardText);
  cardBody.appendChild(btnResart);
  victory.appendChild(cardBody);
  winCard?.append(victory)


  if (btnResart) {
    btnResart.addEventListener("click", () => {
      location.reload()
    })
  }
}